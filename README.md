# Eliecer Aguilar - Universidad Tecnologica de Panamá
***
 ### Bienvenido a mi primer repositorio del curso de git Banco general
 ***

## [BG-UTP] CURSO DE GIT - Programa Fomento al Talento Integral TIC

### [Ejercicio 0 - Iniciar un proyecto git/git init/git clone/.gitignore](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/00.Ejercicio-0/Instrucciones.md)

> Cómo empezar un proyecto *git*, iniciar desde local o clonar un repositorio existente y en nuestro local. Además, veremos cómo ignorar ficheros o directorios para no subirlos al repositorio.



### [Ejercicio 1 - Uso de stage/commit/push](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/01.Ejercicio-1/Instrucciones.md)

> Primera modificación de nuestro código, *commit* y subida al repositorio. Veremos qué es el *stage* y los diferentes estados que pasa un fichero desde que se añade al repositorio, modifica y acaba en el repositorio remoto.

### [Ejercicio 2 - Uso de pull/fecth, como mantener actualizados mis cambios](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/02.Ejercicio-2/Instrucciones.md)

> Primera bajada de código del repositorio remoto, en principio haremos una sincronización sencilla sin conflictos.

### [Ejercicio 3 - Gestión de ramas Locales/Remotas](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/03.Ejercicio-3/Instrucciones.md)

> Crear una rama, cambio de ramas, listar ramas, saber en qué rama estamos trabajando, *trackear* una rama de un repositorio remoto.

### [Ejercicio 4 - Gestión de repositorios remotos](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/04.Ejercicio-4/Instrucciones.md)

> Gestión de repositorios remotos, el *origin* no lo es todo, *git* es un sistema de control de versiones distribuido, aprovechemos su potencia. Añadir, modificar, remover punteros remotos para poder hacer *push/pull* de cualquiera de ellos en cualquier momento que para el *workflow* de trabajo sea interesante.

### [Ejercicio 5 - ¿Qué es y para qué usar el stash?](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/05.Ejercicio-5/Instrucciones.md)

> El *stash*, es una pila que nos puede ayudar en multitud de operaciones. Sabrás identificar los casos más comunes en los que nos coolaboraría, permitiendonos añadir a la misma nuestros cambios, sacarlos y aplicarlos cuando estemos posicionados donde necesitamos.

### [Ejercicio 6 - Volver atrás tus cambios](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/06.Ejercicio-6/Instrucciones.md)

> Vale, me he equivocado añadiendo al *stage* un fichero, o me he equivocado *commiteando* un fichero que no quiero añadir en mi *push*, ¿qué debo hacer? Aprenderemos a revisar el log y mover el puntero del estado de cada fichero, a quitarlo del *stage*, a *resetear* su estado.

### [Ejercicio 7 - Rebase y rebase interactivo](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/07.Ejercicio-7/Instrucciones.md)

> Una de las cosas más importantes para un buen flujo de trabajo es conocer toda la potencia que *git* nos pone a nuestro alcance, dos de las más potentes son el *rebase* y el **rebase interactivo**, aprenderemos qué es, por qué se usa y cuándo es el momento en el flujo para hacerlo.

### [Ejercicio 8 - El merge y el mergetool](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/08.Ejercicio-8/Instrucciones.md)

> Vale la cosa ha ido mal, es normal, tengo conflictos al actualizarme en algún fichero, tengo que *mergear*, aprenderemos como hacerlo, usar herramientas gráficas y como configurarlas.

### [Ejercicio 9 - El uso de cherry-pick](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/00.Material/01.Instrucciones/09.Ejercicio-9/Instrucciones.md)

> Si quieres mover ciertos commits entre las ramas que tengas, `cherry-pick` es tu comando. Aprende en este apartado a seleccionar un commit a partir de su hash para llevarlo a la rama en la que lo necesites.

### [Referencias y enlaces de interés](https://gitlab.com/bg-utp-repository/devops-y-control-de-versiones-git/-/blob/dev/referencias.md)

> Urls de manuales, documentación oficial.
